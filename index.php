<?php
require_once 'classes/Employee.php';
require_once 'classes/User.php';
require_once 'classes/Rectangle.php';
require_once 'classes/Student.php';
require_once 'classes/EmployeeNew.php';
require_once 'classes/City.php';
require_once 'classes/StudentNew.php';
require_once 'classes/Arr.php';
require_once 'classes/UserNew.php';
require_once 'classes/ArrNew.php';
require_once 'classes/ArrNew2.php';
require_once 'classes/ArrayAvgHelper.php';
require_once 'classes/UserNew2.php';
require_once 'classes/EmployeeNew2.php';

// Exercise 2 - Создайте объект класса Employee, затем установите его свойства
// в следующие значения - имя 'john', возраст 25, зарплата 1000.

$employee1 = new Employee();
$employee1->name = 'john';
$employee1->age = 25;
$employee1->salary = 1000;

// Exercise 3 - Создайте второй объект класса Employee, установите его свойства
// в следующие значения - имя 'eric', возраст 26, зарплата 2000.
$employee2 = new Employee();
$employee2->name = 'eric';
$employee2->age = 26;
$employee2->salary = 2000;

// Exercise 4 - Выведите на экран сумму зарплат созданных юзеров.
echo $employee1->salary + $employee2->salary;
echo '<br>';

// Exercise 5 - Выведите на экран сумму возрастов созданных юзеров.
echo $employee1->age + $employee2->age;
echo '<br>';

// Exercise 6 - реализуйте такой же класс User с методом show().
$user = new User;
$user->name = 'john';
$user->age = 25;
echo $user->show('hello');
echo '<br>';


// Exercise 7 - Сделайте в классе Employee метод getName, который будет возвращать имя работника.
echo $employee1->getName();
echo '<br>';

// Exercise 8 - Сделайте в классе Employee метод getAge, который будет возвращать возраст работника.
echo $employee1->getAge();
echo '<br>';

// Exercise 9 - Сделайте в классе Employee метод getSalary, который будет возвращать зарплату работника.
echo $employee1->getSalary();
echo '<br>';

// Exercise 10 - Сделайте в классе Employee метод checkAge, который будет проверять то,
// что работнику больше 18 лет и возвращать true, если это так, и false, если это не так.
var_dump($employee1->checkAge());
echo '<br>';

// Exercise 11 - Создайте два объекта класса Employee, запишите в их свойства какие-либо значения.
// С помощью метода getSalary найдите сумму зарплат созданных работников.
echo $employee1->getSalary()  + $employee2->getSalary();
echo '<br>';

// Exercise 12 - Создайте объект класса User с именем 'john' и возрастом 25.
// С помощью метода setAge поменяйте возраст на 30. Выведите новое значение возраста на экран.
$user->setAge(30);
echo $user->age;
echo '<br>';

// Exercise 13 - Модифицируйте метод setAge так, чтобы он вначале проверял, что переданный возраст больше или равен 18.
// Если это так - пусть метод меняет возраст пользователя, а если не так - то ничего не делает.
$user->setAge(16);
echo $user->age;
echo '<br>';

// Exercise 14 - Сделайте класс Employee, в котором будут следующие свойства работника - name, salary.
// Сделайте метод doubleSalary, который текущую зарплату будет увеличивать в 2 раза.
$employee1->doubleSalary();
echo $employee1->getSalary();
echo '<br>';

// Exercise 15 - Сделайте класс Rectangle, в котором в свойствах будут записаны ширина и высота прямоугольника.
$rectangle = new Rectangle();
echo $rectangle->width = 20;
echo '<br>';
echo $rectangle->height = 10;
echo '<br>';

// Exercise 16 - В классе Rectangle сделайте метод getSquare,
// который будет возвращать площадь этого прямоугольника.
echo $rectangle->getSquare();
echo '<br>';

// Exercise 17 - В классе Rectangle сделайте метод getPerimeter,
// который будет возвращать периметр этого прямоугольника.
echo $rectangle->getPerimeter();
echo '<br>';

// Exercise 18 - Создайте объект этого класса User проверьте работу методов setAge и addAge.
$user ->setAge(24);
echo $user->age;
echo '<br>';
$user ->addAge(12);
echo $user->age;
echo '<br>';

// Exercise 20 - Добавьте также метод subAge, который будет выполнять
// уменьшение текущего возраста на переданное количество лет.
$user ->subAge(5);
echo $user->age;
echo '<br>';

// Exercise 21 - Попробуйте вызвать метод isAgeCorrect снаружи класса.
// Убедитесь, что это будет вызывать ошибку.
/*$user ->isAgeCorrect(20);*/
echo $user->age;
echo '<br>';

// Exercise 22 - В классе Student сделайте public метод transferToNextCourse,
// который будет переводить студента на следующий курс.
$student = new Student();
$student->setName("Bob");
$student->setCourse(1);
$student->transferToNextCourse();
echo $student->course;
echo '<br>';


// Exercise 23 - Создайте объект класса Employee с именем 'eric', возрастом 25, зарплатой 1000.
$employeeNew1 = new EmployeeNew('eric', 'johnson', 25, 1000);
echo 'Name - ' . $employeeNew1->getName() .'; Age: ' . $employeeNew1->getAge() . '; Salary: ' . $employeeNew1->getSalary();
echo '<br>';

// Exercise 24 - Создайте объект класса Employee с именем 'kyle', возрастом 30, зарплатой 2000.
$employeeNew2 = new EmployeeNew('kyle', 'parker', 30, 2000);
echo 'Name - ' . $employeeNew2->getName() .'; Age: ' . $employeeNew2->getAge() . '; Salary: ' . $employeeNew2->getSalary();
echo '<br>';

// Exercise 25 - Выведите на экран сумму зарплат созданных вами юзеров.
echo $employeeNew1->getSalary() + $employeeNew2->getSalary();
echo '<br>';

// Exercise 26 - Создайте 5 объектов класса City, заполните их данными и запишите в массив.
$tokyo = new City('Tokyo', 37435191, 1603);
$delhi = new City('Delhi', 29399141, 1206);
$shanghai = new City('Shanghai', 26317104, 746);
$sao_paulo = new City('Sao Paulo', 21846507, 1920);
$mexico_city = new City('Mexico City', 21671908, 1521);
$cities = array();
$cities[] = $tokyo;
$cities[] = $delhi;
$cities[] = $shanghai;
$cities[] = $sao_paulo;
$cities[] = $mexico_city;
var_dump($cities);
echo '<br>';


// Exercise 26 - Переберите созданный вами массив с городами циклом и выведите города и их население на экран.

foreach($cities as $city)
{
    echo 'City: ' . $city->name . ', Population: '. $city->population . ';<br>';
}

// Exercise 27 - Модифицируйте метод transferToNextCourse так, чтобы при переводе на новый курс
// выполнялась проверка того, что новый курс не будет больше 5.
$studentNew = new StudentNew('jonson');
$studentNew->transferToNextCourse();
echo $studentNew->getCourse();
echo '<br>';


// Exercise 27 - Реализуйте класс Arr. Метод add вашего класса параметром должен принимать массив чисел.
// Все числа из этого массива должны добавляться в конец массива $this->numbers.
$some_array1 = range(1,5);
$some_array2 = range(6,10);
$arr = new Arr();
$arr->add($some_array1);
$arr->add($some_array2);
var_dump($arr->getNumbers());
echo '<br>';


// Exercise 28 - Реализуйте также метод getAvg, который будет находить среднее арифметическое чисел.
var_dump($arr->getAvg());
echo '<br>';

// Exercise 29 - Сделайте класс City, в котором будут следующие свойства - name,
// foundation (дата основания), population (население). Создайте объект этого класса.
$city = new City('Kyiv', 2884000, 482);

// Exercise 30 - Пусть дана переменная $props, в которой хранится массив названий свойств класса City.
// Переберите этот массив циклом foreach и выведите на экран значения соответствующих свойств.
$props = ['name', 'population', 'foundation'];
foreach ($props as $prop)
{
    echo $city->$prop . '<br>';
}


// Exercise 31 - Выведите все свойства с массива не используя цыкл.
echo $city->{$props[0]} . '<br>';
echo $city->{$props[1]} . '<br>';
echo $city->{$props[2]} . '<br>';


// Exercise 32 - Пусть массив $methods будет ассоциативным с ключами method1 и method2.
// Выведите имя и возраст пользователя с помощью этого массива.
$userNew = new UserNew('bill', 27);
$methods = ['method1' => 'getName', 'method2' => 'getAge'];
echo $userNew->{$methods['method1']}() . '<br>';
echo $userNew->{$methods['method2']}() . '<br>';


// Exercise 33 - Не подсматривая в мой код реализуйте такой же класс Arr
// и вызовите его метод getSum сразу после создания объекта.
echo (new ArrNew([1, 2, 3]))->getSum() . "<br>";


// Exercise 34 - самостоятельно реализуйте такой же класс Arr, методы которого будут вызываться в виде цепочки.
echo (new ArrNew2)->add(1)->add(2)->push([3,4,5])->getSum() . '<br>';


// Exercise 35 - Напишите реализацию методов класса ArrayAvgHelper
$arr = range(1,5);
$arrayAvgHelper = new ArraySumHelper();
echo $arrayAvgHelper->getAvg1($arr) . '<br>';
echo $arrayAvgHelper->getAvg2($arr) . '<br>';
echo $arrayAvgHelper->getAvg3($arr) . '<br>';


// Exercise 36 - реализуйте такие же классы User, Employee.
$employee = new EmployeeNew2();

$employee->setSalary(2000);
$employee->setName('bill');
$employee->setAge(29);

echo $employee->getSalary() . '<br>';
echo $employee->getName() . '<br>';
echo $employee->getAge() . '<br>';


