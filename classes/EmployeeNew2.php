<?php
require_once 'classes/UserNew2.php';
class EmployeeNew2 extends UserNew2
{
    private $salary;

    public function getSalary()
    {
        return $this->salary;
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

}