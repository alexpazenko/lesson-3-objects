<?php

class Student
{
    public $name;
    public $course;

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setCourse(int $course)
    {
        if ($this->isCourseCorrect($course)) {
            $this->course = $course;
        }
    }

    public function transferToNextCourse()
    {
        if ($this->istransferToNext()) {
            $this->course += 1;
        }
    }

    private function istransferToNext()
    {
        if ($this->course < 5) {
            return true;
        } else {
            return false;
        }
    }

    private function isCourseCorrect(int $course)
    {
        if ($course >= 1 && $course <= 5) {
            return true;
        } else {
            return false;
        }
    }
}