<?php

class StudentNew
{
    private $name;
    private $course;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->course = 1;

    }

    public function getName()
    {
        return $this->name;
    }

    public function getCourse()
    {
        return $this->course;
    }

    public function transferToNextCourse()
    {
        if ($this->istransferToNext()) {
            $this->course += 1;
        }
    }

    private function istransferToNext()
    {
        if ($this->course < 5) {
            return true;
        } else {
            return false;
        }
    }
}