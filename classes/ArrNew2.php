<?php

class ArrNew2
{
    private $numbers = [];

    public function add($number)
    {
        $this->numbers[] = $number;
        return $this;
    }

    public function push(array $numbers)
    {
        $this->numbers = array_merge($this->numbers, $numbers);
        return $this;
    }

    public function getSum()
    {
        return array_sum($this->numbers);
    }
}