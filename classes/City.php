<?php

class City
{
    public $name;
    public $population;
    public $foundation;

    public function __construct(string $name, int $population, $foundation)
    {
        $this->name = $name;
        $this->population = $population;
        $this->foundation = $foundation;
    }

    /*public function getName()
    {
        return $this->name;
    }

    public function getPopulation()
    {
        return $this->population;
    }*/
}