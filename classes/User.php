<?php

class User
{
    public $name;
    public $age;

    public function show(string $str)
    {
        return $str. '!!!';
    }

    public function setAge(int $age)
    {
        if ($this->isAgeCorrect($age)) {
            $this->age = $age;
        }
    }

    public function addAge(int $years)
    {
        $newAge = $this->age + $years;

        if ($this->isAgeCorrect($newAge)) {
            $this->age = $newAge;
        }
    }

    public function subAge(int $years)
    {
        $newAge = $this->age - $years;

        if ($this->isAgeCorrect($newAge)) {
            $this->age = $newAge;
        }
    }


    private function isAgeCorrect(int $age)
    {
        if ($age >= 18 && $age <= 60) {
            return true;
        } else {
            return false;
        }
    }
}