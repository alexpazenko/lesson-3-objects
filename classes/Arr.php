<?php

class Arr
{
    private $numbers = [];

    public function add(array $num_array)
    {
        $this->numbers = array_merge($this->numbers, $num_array);
    }

    public function getNumbers()
    {
        return $this->numbers;
    }
    public function getAvg()
    {
        return array_sum($this->numbers)/count($this->numbers);
    }
}