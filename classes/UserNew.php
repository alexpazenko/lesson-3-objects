<?php

class UserNew
{
    private $name;
    private $age;

    public function __construct(string $name, int $age)
    {
        $this->name = $name;
        if ($this->isAgeCorrect($age)) {
        $this->age = $age;
        }
    }

    public function getName()
    {
       return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    private function isAgeCorrect(int $age)
    {
        if ($age >= 18 && $age <= 60) {
            return true;
        } else {
            return false;
        }
    }
}