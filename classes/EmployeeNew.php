<?php

class EmployeeNew
{
    private $name;
    private $surname;
    private $age;
    private $salary;

    public function __construct(string $name, string $surname, int $age, int $salary)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->age = $age;
        $this->salary = $salary;

    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setAge($age)
    {
        if ($this->isAgeCorrect($age)) {
        $this->age = $age;
        }
    }

    public function getAge()
    {
        return $this->age;
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }


    public function getSalary()
    {
        return $this->salary;
    }

    private function isAgeCorrect($age)
    {
        if ($age >= 1 && $age <=100) {
            return true;
        } else {
            return false;
        }
    }
}